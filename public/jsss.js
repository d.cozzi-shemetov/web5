/*jslint browser */
/*global window */
function finalPrice() {
    let price = document.getElementById("spisok");
    let kolvo = document.getElementById("hj");
    let s = 0;
    if (parseInt(price.value) === 2000) {
        document.getElementById("spisok1").style.display = "block";
        document.getElementById("spisok2").style.display = "none";
        s = parseInt(price.value);
        let rad1 = document.getElementById("s11");
        let rad2 = document.getElementById("s12");
        s += ((rad1.checked === true) ? parseInt(rad1.value) : 0);
        s += ((rad2.checked === true) ? parseInt(rad2.value) : 0);
	
    }
    else {
        if (parseInt(price.value) === 1000) {
            document.getElementById("spisok1").style.display = "none";
            document.getElementById("spisok2").style.display = "block";
            s = parseInt(price.value);
            let ch = document.getElementById("s2");
            s += ((ch.checked === true) ? parseInt(ch.value) : 0);
        }
        else {
            if (parseInt(price.value) === 5000) {
                document.getElementById("spisok1").style.display = "none";
                document.getElementById("spisok2").style.display = "none";
                s = parseInt(price.value);
            }
            else {
                document.getElementById("spisok1").style.display = "none";
                document.getElementById("spisok2").style.display = "none";
                s = 0;
            }
        }
    }
    document.getElementById("stoim").innerHTML = s * (kolvo.value);
}
window.addEventListener("DOMContentLoaded", function(event) {
    let rdiv = document.getElementById("spisok1");
    rdiv.style.display = "none";

    let cdiv = document.getElementById("spisok2");
    cdiv.style.display = "none";

    let radios = document.getElementsByName("opt");
    radios.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
            let r = event.target;
            console.log(r.value);
            finalPrice();
        });
    });

    let checkboxes = document.getElementsByName("check");
    checkboxes.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
            let c = event.target;
            console.log(c.value);
        finalPrice();
        });
    });
    let s = document.getElementById("spisok");
    let select = s[0];
    select.addEventListener("change", function(event) {
        let tt = event.target;
        console.log(tt.value);
        finalPrice();
    });
    finalPrice();
});
window.addEventListener("change", finalPrice);
window.addEventListener("keyup", finalPrice);
